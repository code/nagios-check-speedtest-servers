check\_speedtest\_servers
=========================

Check that an Oookla Speedtest server with a specified URL and/or host is
present on the public list of servers.

    # check by host:port pair
    $ ./check_speedtest_servers --host example.com:8080
    # check by "legacy upload" URL
    $ ./check_speedtest_servers --url http://example.com/upload.php
    # check by both (AND, not OR)
    $ ./check_speedtest_servers --host example.com:8080 --url http://example.com/upload.php

Thanks
------

This was written on company time with my employer [Inspire Net][1], who has
generously allowed me to open source it.

License
-------

Copyright (c) [Tom Ryder][2]. Distributed under [MIT License][3].

[1]: https://www.inspire.net.nz/
[2]: https://sanctum.geek.nz/
[3]: https://opensource.org/licenses/MIT
